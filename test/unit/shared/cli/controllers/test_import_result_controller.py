import unittest
from unittest.mock import Mock

from app.order.models import Order
from app.shared.cli.controllers.import_results_controller import ImportDataRow, ImportResultsController
from app.user.models import User


class TestImportResultsController(unittest.TestCase):

    def setUp(self):
        self.data = [
            ImportDataRow({
                "patientId": "1",
                "patientName": "",
                "patientSurname": "",
                "patientBirthDate": "",
                "patientSex": "male",
                "orderId": "2",
                "testName": "",
                "testReference": "",
                "testValue": "",
            }),
            ImportDataRow({
                "patientId": "2",
                "patientName": "",
                "patientSurname": "",
                "patientBirthDate": "",
                "patientSex": "male",
                "orderId": "2",
                "testName": "",
                "testReference": "",
                "testValue": "",
            }),
            ImportDataRow({
                "patientId": "1",
                "patientName": "",
                "patientSurname": "",
                "patientBirthDate": "",
                "patientSex": "male",
                "orderId": "2",
                "testName": "",
                "testReference": "",
                "testValue": "",
            }),
        ]
        self.db = Mock()

    def test_init(self):
        controller = ImportResultsController(self.data, self.db)
        self.assertEqual(len(controller.results), 2)

    def test_exec(self):
        self.db.query.return_value.get.return_value = None

        controller = ImportResultsController(self.data, self.db)
        controller.exec()

        self.assertEqual(self.db.add.call_count, 2)
        self.assertTrue(self.db.commit.called)

    def test_check_if_user_exists(self):
        user = Mock()
        self.db.query.return_value.get.return_value = user

        controller = ImportResultsController(self.data, self.db)
        with self.assertRaises(ValueError):
            controller.check_if_exists(User, 1)

        self.db.query.return_value.get.return_value = None
        controller.check_if_exists(User, 1)

    def test_check_if_orer_exists(self):
        order = Mock()
        self.db.query.return_value.get.return_value = order

        controller = ImportResultsController(self.data, self.db)
        with self.assertRaises(ValueError):
            controller.check_if_exists(Order, 1)

        self.db.query.return_value.get.return_value = None
        controller.check_if_exists(Order, 1)


if __name__ == '__main__':
    unittest.main()
