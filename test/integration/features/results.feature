Feature: Results feature

  Scenario: return 401 when missing JWT
    Given the fixtures "user" are loaded
    When I get api/results
    Then the HTTP status is 401

  Scenario: return response for authenticated user
    Given the fixtures "results" are loaded
    And I have a valid token for user with id 1
    When I get api/results
    Then the HTTP status is 200
    Then the response match
    """
      {
        "patient": {
          "id": 1,
          "name": "name",
          "surname": "lastname",
          "sex": "m",
          "birthDate": "01-01-1993"
        },
        "orders": [
          {
            "orderId": "1",
            "results": [
              {
                "name": "name",
                "value": "value",
                "reference": "reference"
              }
            ]
          }
        ]
      }
    """