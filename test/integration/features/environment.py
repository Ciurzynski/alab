from behave import fixture, use_fixture
from fastapi import Request, HTTPException
from fastapi.testclient import TestClient
from jose import jwt
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy_utils import database_exists, create_database
from starlette import status

from app.config import get_env
from app.db import Base
from app.dependencies import get_db
from app.main import app
from app.shared.security import get_current_user
from app.user.models import User

env = get_env()

SQLALCHEMY_DATABASE_URL = f"postgresql://{env.postgres_user}:{env.postgres_password}@{env.postgres_host}/{env.postgres_test_db}"

engine = create_engine(SQLALCHEMY_DATABASE_URL)

if not database_exists(engine.url):
    create_database(engine.url)

TestingSessionLocal = sessionmaker(autocommit=False, autoflush=False,
                                   bind=engine)


def override_get_db():
    try:
        db = TestingSessionLocal()
        yield db
    finally:
        db.close()


def override_auth(request: Request):
    credentials_exception = HTTPException(
        status_code=status.HTTP_401_UNAUTHORIZED,
        detail="Could not validate credentials",
        headers={"WWW-Authenticate": "Bearer"},
    )
    if not request.headers.get("authorization"):
        raise credentials_exception
    payload = jwt.decode(request.headers.get("authorization").split()[1], env.secret, algorithms=[env.algorithm])
    username: str = payload.get("sub")
    user = next(override_get_db(), None).query(User).filter(User.login == username).first()
    return user


app.dependency_overrides[get_db] = override_get_db
app.dependency_overrides[get_current_user] = override_auth


@fixture
def fastapi_client(context, *args, **kwargs):
    context.db = next(override_get_db(), None)
    context.client = TestClient(app)
    yield context.client


def before_feature(context, feature):
    Base.metadata.drop_all(bind=engine)
    Base.metadata.create_all(bind=engine)
    use_fixture(fastapi_client, context)
