from app.order.models import Order, OrderItem
from app.user.models import User


def get_fixtures():
    fixtures = []

    user = User()
    user.id = 1
    user.name = "name"
    user.lastname = "lastname"
    user.login = "login"
    user.password = "test"
    user.sex = "m"
    user.brith_date = "01-01-1993"
    user.orders = []

    order1 = Order(id=1)

    order_item = OrderItem(name="name", value="value", reference="reference")
    order1.items.append(order_item)
    user.orders.append(order1)

    fixtures.append(user)

    return fixtures
