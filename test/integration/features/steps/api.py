import json

from behave import *
from jose import jwt

from app.config import get_env
from app.user.models import User

env = get_env()


def jwt_for(user):
    """
    Generate the payload for a JWT for the given userid
    """
    return {"sub": user.login}


def make_token(secret, payload):
    """
    Encode and sign a JWT with the given payload and secret
    """
    return jwt.encode(payload, secret, algorithm="HS256")


@given("I have a valid token for user with id {userid}")
def step_impl(context, userid):
    """
    Make a valid token and save it in the context
    """
    user = context.db.query(User).get(int(userid))
    context.token = make_token(env.secret, jwt_for(user))


@when("I get {path}")
def step_impl(context, path):
    """
    Make a HTTP GET request

    Uses the token from the context for the Authorization header, if there is
    one.
    """
    headers = {}
    if hasattr(context, "token"):
        headers["Authorization"] = "Bearer " + context.token
    context.response = context.client.get(path, headers=headers)


@when("I post to {path}")
def step_impl(context, path):
    """
    Make a POST request

    Uses the token from the context for the Authorization header, if there is
    one.
    """
    headers = {}
    if hasattr(context, "token"):
        headers["Authorization"] = "Bearer " + context.token
    context.response = context.client.post(
        path,
        data=context.text,
        headers=headers
    )


@when("I patch {path}")
def step_impl(context, path):
    """
    Make a PATCH request

    Uses the token from the context for the Authorization header, if there is one.
    """
    headers = {}
    if hasattr(context, "token"):
        headers["Authorization"] = "Bearer " + context.token
    context.response = context.client.patch(
        path,
        data=context.text,
        headers=headers
    )


@when("I delete {path}")
def step_impl(context, path):
    """
    Make a DELETE request

    Uses the token from the context for the Authorization header, if there is one.
    """
    headers = {}
    if hasattr(context, "token"):
        headers["Authorization"] = "Bearer " + context.token

    context.response = context.client.delete(path, headers=headers)


@then("the HTTP status is {status}")
def step_impl(context, status):
    """
    Assert the value of the HTTP status of the last response
    """
    actual = context.response.status_code
    assert actual == int(status), "Expected %s, got %d" % (status, actual)


@then("the response text is")
def step_impl(context):
    """
    Assert that the response text is exactly a particular value
    """
    actual = context.response.text
    assert context.text == actual, \
        "Expected %s, got %s" % (context.text, actual)


@then('the response header "{header}" contains "{substring}"')
def step_impl(context, header, substring):
    actual = context.response.headers[header]
    assert substring in actual, "Header %s (%s) should contain %s" % (
        header,
        actual,
        substring,
    )


def contains(required, actual):
    """
    True if actual is a superset of required
    """
    if isinstance(required, dict) and isinstance(actual, dict):
        for key in required:
            if not contains(required[key], actual[key]):
                return False
        return True
    if isinstance(required, list) and isinstance(actual, list):
        all_found = True
        for required_item in required:
            found = False
            for actual_item in actual:
                found = found or contains(required_item, actual_item)
            all_found = all_found and found
        return all_found
    return required == actual


@then("dump the response headers")
def step_impl(context):
    """
    Spit the response headers out of stdout

    This can be useful when debugging. It's not really something that should be left in
    a scenario.
    """
    print(context.response.headers)


@then("dump the response body")
def step_impl(context):
    """
    Spit the response body out of stdout

    This can be useful when debugging. It's not really something that should be left in
    a scenario.
    """
    print(context.response.text)


@then("the response match")
def step_impl(context):
    actual = json.loads(context.response.text)
    required = json.loads(context.text)
    assert contains(required, actual), "Expected response not match"
