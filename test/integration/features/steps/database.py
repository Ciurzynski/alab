from behave import *

from test.integration.features.fixtures import results


@given('the fixtures "{file}" are loaded')
def load_fixtures(context, file):
    if file == 'results':
        context.db.add_all(results.get_fixtures())
    context.db.commit()
