# ALAB recruitment task

A project created for a recruitment task for ALAB. It is a simple API that returns patient test results. The application
has functionality to import results from a csv file. More on the requirements can be found in the REQUIREMENTS.md file 

**Note for the reviewer:**
```text
Based on the example answer (results.json), I assumed that the returned data should be filtered for the currently 
logged-in user (based on the token). To get the token you need the login and password. For the purpose of the task, 
I assumed that the login would be the patient's first and last name and the password would be their date of birth. 
I know that this is not an optimal solution and that it has a few security gaps, but I have decided that for the 
purposes of the recruitment task this functionality is sufficient.
```

### Installation:

1. copy .env.dist to the .env file
2. docker-compose up --build -d
3. docker-compose exec app python -m alembic upgrade head

### Database

The application is available at localhost. User, port and password information can be found in the `.env.dist` file

**NOTE:**

The database schema is located in the db directory. However, it is not necessary for importing. The application uses the 
alembic migration `docker-compose exec app python -m alembic upgrade head`.

### API

After the installation steps, the api is available at http://localhost:80. 

**Api documentation**:  http://localhost:80/docs 

### CLI
The application has a CLI tool. Example of running a help command:

```shell
docker-compose exec app python app/cli.py --help
```

#### How do I import patient data?
The test data for import can be found in the data directory. The folder is used as a volume in the `app` service, i.e. 
data from this folder is available inside the container. To import patient data run the following command:
```shell
docker-compose exec app python app/cli.py results import-results data/results.csv
```

### Testing

The application has basic unit and integration tests. Below you will find instructions on how to run the tests

#### Unit

```shell
docker-compose exec app python -m unittest
```

#### Integration (behave)

```shell
docker-compose exec app python -m behave ./test/integration/features
```

### Code inspection

The application has a tool for checking code quality. To run it, execute the following command
```shell
docker-compose exec app python -m flake8 --config .flake8
```

#### Troubleshooting:

1.
    **PROBLEM**: app module not found

    **SOLUTION**: before running cli, execute the command:

```shell
export PYTHONPATH="$PYTHONPATH:$(pwd)"
```
