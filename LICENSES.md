| Name              | Version  | License                                             |
|-------------------|----------|-----------------------------------------------------|
| Mako              | 1.3.2    | MIT License                                         |                                         
| MarkupSafe        | 2.1.5    | BSD License                                         |                                         
| PyYAML            | 6.0.1    | MIT License                                         |                                         
| Pygments          | 2.17.2   | BSD License                                         |                                         
| SQLAlchemy        | 2.0.27   | MIT License                                         |                                         
| SQLAlchemy-Utils  | 0.41.1   | BSD License                                         |                                         
| alembic           | 1.13.1   | MIT License                                         |                                         
| annotated-types   | 0.6.0    | MIT License                                         |                                         
| anyio             | 4.3.0    | MIT License                                         |                                         
| bcrypt            | 4.1.2    | Apache Software License                             |                             
| behave            | 1.2.6    | BSD License                                         |                                         
| certifi           | 2024.2.2 | Mozilla Public License 2.0 (MPL 2.0)                |                
| cffi              | 1.16.0   | MIT License                                         |                                         
| click             | 8.1.7    | BSD License                                         |                                         
| colorama          | 0.4.6    | BSD License                                         |                                         
| cryptography      | 42.0.5   | Apache Software License; BSD License                |                
| ecdsa             | 0.18.0   | MIT                                                 |                                                 
| exceptiongroup    | 1.2.0    | MIT License                                         |                                         
| fastapi           | 0.110.0  | MIT License                                         |                                         
| greenlet          | 3.0.3    | MIT License                                         |                                         
| h11               | 0.14.0   | MIT License                                         |                                         
| httpcore          | 1.0.4    | BSD License                                         |                                         
| httptools         | 0.6.1    | MIT License                                         |                                         
| httpx             | 0.27.0   | BSD License                                         |                                         
| idna              | 3.6      | BSD License                                         |                                         
| install           | 1.3.5    | MIT License                                         |                                         
| markdown-it-py    | 3.0.0    | MIT License                                         |                                         
| mdurl             | 0.1.2    | MIT License                                         |                                         
| parse             | 1.20.1   | MIT License                                         |                                         
| parse-type        | 0.6.2    | MIT License                                         |                                         
| passlib           | 1.7.4    | BSD                                                 |                                                 
| psycopg2          | 2.9.9    | GNU Library or Lesser General Public License (LGPL) | 
| pyasn1            | 0.5.1    | BSD License                                         |                                         
| pycparser         | 2.21     | BSD License                                         |                                         
| pydantic          | 2.6.3    | MIT License                                         |                                         
| pydantic_core     | 2.16.3   | MIT License                                         |                                         
| python-dotenv     | 1.0.1    | BSD License                                         |                                         
| python-jose       | 3.3.0    | MIT License                                         |                                         
| python-multipart  | 0.0.9    | Apache Software License                             |                             
| rich              | 13.7.1   | MIT License                                         |                                         
| rsa               | 4.9      | Apache Software License                             |                             
| shellingham       | 1.5.4    | ISC License (ISCL)                                  |                                  
| six               | 1.16.0   | MIT License                                         |                                         
| sniffio           | 1.3.1    | Apache Software License; MIT License                |                
| starlette         | 0.36.3   | BSD License                                         |                                         
| typer             | 0.9.0    | MIT License                                         |                                         
| typing_extensions | 4.10.0   | Python Software Foundation License                  |                  
| uvicorn           | 0.27.1   | BSD License                                         |                                         
| uvloop            | 0.19.0   | Apache Software License; MIT License                |                
| watchfiles        | 0.21.0   | MIT License                                         |                                         
| websockets        | 12.0     | BSD License                                         |  