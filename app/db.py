from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
from .config import get_env

env = get_env()
engine = create_engine(
    f"postgresql://{env.postgres_user}:{env.postgres_password}" +
    f"@{env.postgres_host}/{env.postgres_db}"
)

SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)

Base = declarative_base()
