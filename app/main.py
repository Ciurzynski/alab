from fastapi import FastAPI
from app.api import results, auth

app = FastAPI()

app.include_router(results.router, prefix="/api")
app.include_router(auth.router, prefix="/api")
