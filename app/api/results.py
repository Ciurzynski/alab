from typing import List, Annotated

from fastapi import APIRouter, Depends
from pydantic import BaseModel

from app.order.schema import OrderResponse
from app.shared.security import get_current_user
from app.user.models import User
from app.user.schema import PatientResponse


class ResultsResponse(BaseModel):
    patient: PatientResponse
    orders: List[OrderResponse]


router = APIRouter(
    prefix="/results",
    tags=["results"],
    responses={404: {"description": "Not found"}}
)


@router.get("", response_model=ResultsResponse, tags=["results"])
def get_results(user: Annotated[User, Depends(get_current_user)]):
    orders = list(map(lambda o: OrderResponse.from_model(o), user.orders))
    return ResultsResponse(patient=PatientResponse.from_model(user), orders=orders)
