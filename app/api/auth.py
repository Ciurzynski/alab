from typing import Annotated

from fastapi import Depends, HTTPException, status, APIRouter
from fastapi.security import OAuth2PasswordRequestForm

from app.db import SessionLocal
from app.shared.schema import Token
from app.shared.security import authenticate_user, create_access_token

session = SessionLocal()
router = APIRouter(
    prefix="/login",
    tags=["auth"]
)


@router.post("")
async def login(
        form_data: Annotated[OAuth2PasswordRequestForm, Depends()]
) -> Token:
    user = authenticate_user(form_data.username, form_data.password)
    if not user:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Incorrect username or password",
            headers={"WWW-Authenticate": "Bearer"},
        )
    access_token = create_access_token(data={"sub": user.login})
    return Token(access_token=access_token, token_type="bearer")
