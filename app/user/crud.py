from app.db import SessionLocal
from app.user.models import User as UserModel

session = SessionLocal()


def get_by_login(login: str):
    db_user = session.query(UserModel).filter(UserModel.login == login).first()
    return db_user
