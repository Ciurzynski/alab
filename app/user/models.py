from sqlalchemy import Column, String, Integer, Date
from sqlalchemy.orm import relationship

from app.db import Base


class User(Base):
    __tablename__: str = "user"

    id = Column(Integer, primary_key=True)
    name = Column(String(40))
    lastname = Column(String(40))
    login = Column(String(80))
    password = Column(String(120))
    sex = Column(String(1))
    brith_date = Column(Date)
    orders = relationship("Order", uselist=True, backref="owner")
