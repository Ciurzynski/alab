from pydantic import BaseModel

from app.user.models import User


class PatientResponse(BaseModel):
    id: int
    name: str
    surname: str
    sex: str
    birthDate: str

    @staticmethod
    def from_model(user: User):
        return PatientResponse(
            id=user.id,
            name=user.name,
            surname=user.lastname,
            sex=user.sex,
            birthDate=user.brith_date.strftime('%d-%m-%Y')
        )
