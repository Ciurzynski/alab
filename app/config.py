from functools import lru_cache
from pydantic.v1 import BaseSettings


class Config(BaseSettings):
    app_name: str = "Alab API"
    postgres_user: str = ""
    postgres_db: str = ""
    postgres_test_db: str = ""
    postgres_password: str = ""
    postgres_host: str = ""
    secret: str = ""
    algorithm: str = "HS256"
    access_token_expires_minutes: int = 30

    class Config:
        env_file = ".env"


@lru_cache()
def get_env():
    return Config()
