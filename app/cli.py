#!/usr/bin/env

import typer

from app.shared.cli.import_results import typer_app

typer = typer.Typer()

typer.add_typer(typer_app, name="results")


if __name__ == "__main__":
    typer()
