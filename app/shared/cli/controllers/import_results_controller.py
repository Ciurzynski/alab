from typing import List, Iterable

from passlib.context import CryptContext

from app.db import SessionLocal, Base
from app.order.models import OrderItem, Order
from app.user.models import User

pwd_context = CryptContext(schemes=["bcrypt"], deprecated="auto")


class ImportDataRow:
    id: int
    firstname: str
    lastname: str
    birth_date: str
    sex: str
    order_id: int
    test_name: str
    test_reference: str
    test_value: str

    def __init__(self, data: dict):
        self.id = int(data.get("patientId"))
        self.firstname = data.get("patientName")
        self.lastname = data.get("patientSurname")
        self.birth_date = data.get("patientBirthDate")
        self.sex = self.get_sex(data.get("patientSex"))
        self.order_id = int(data.get("orderId"))
        self.test_name = data.get("testName")
        self.test_reference = data.get("testReference")
        self.test_value = data.get("testValue")

    def __str__(self):
        return self.id

    @staticmethod
    def get_sex(value: str) -> str:
        """
        I know it's a bit fragile but sufficient for this task
        :param value:
        :return str:
        """
        if value == 'male':
            return 'm'
        elif value == 'female':
            return 'f'
        else:
            raise ValueError(f"Invalid value for sex: {value}")


class ImportOrderItem:

    def __init__(self, row: ImportDataRow):
        self.name = row.test_name
        self.value = row.test_value
        self.reference = row.test_reference

    def to_model(self) -> OrderItem:
        """
        Converts the data to the OrderItem model
        :return OrderItem:
        """
        return OrderItem(
            reference=self.reference,
            value=self.value,
            name=self.name
        )


class ImportOrder:
    id: int
    items: List[ImportOrderItem]

    def __init__(self, id: int):
        self.id = id
        self.items = []

    def add_item(self, item: ImportOrderItem):
        self.items.append(item)

    def to_model(self) -> Order:
        """
        Converts the data to the Order model
        :return Order:
        """
        return Order(id=self.id)


class ImportPatient:

    def __init__(self, row: ImportDataRow):
        self.id = row.id
        self.firstname = row.firstname
        self.lastname = row.lastname
        self.birth_date = row.birth_date
        self.sex = row.sex
        self.orders = {}

    def add_order(self, order: ImportOrder):
        self.orders[order.id] = order

    def get_orders(self) -> Iterable[ImportOrder]:
        return self.orders.values()

    def to_model(self) -> User:
        """
        Converts the data to the User model
        :return User:
        """
        return User(
            id=self.id,
            name=self.firstname,
            lastname=self.lastname,
            login=f"{self.firstname} {self.lastname}",
            password=pwd_context.hash(self.birth_date),
            brith_date=self.birth_date,
            sex=self.sex,
        )


class ImportResultsController:
    results = {}

    def __init__(self, data: List[ImportDataRow], db: SessionLocal) -> None:
        self.db = db

        for data_row in data:
            if data_row.id not in self.results:
                patient = ImportPatient(data_row)
                self.results[data_row.id] = patient
            else:
                patient = self.results[data_row.id]

            if data_row.order_id not in patient.orders:
                order = ImportOrder(data_row.order_id)
                patient.add_order(order)
            else:
                order = patient.orders[data_row.order_id]

            order_item = ImportOrderItem(data_row)
            order.add_item(order_item)

    def exec(self) -> None:
        for patient in self.results.values():
            self.check_if_exists(User, patient.id)
            user = patient.to_model()
            self.db.add(user)
            for order in patient.get_orders():
                self.check_if_exists(Order, int(order.id))
                order_model = order.to_model()
                user.orders.append(order_model)
                for item in order.items:
                    item_model = item.to_model()
                    order_model.items.append(item_model)
        self.db.commit()

    def check_if_exists(self, model: Base, id: int) -> None:
        """
        Checks if the given model exists in the db, if so raise an exception
        :param model:
        :param id:
        :return None:
        """
        if self.db.query(model).get(id) is not None:
            raise ValueError(f"{model} {id} already exists")
