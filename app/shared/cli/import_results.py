import csv
from pathlib import Path

import typer

from app.config import get_env
from app.db import SessionLocal
from app.shared.cli.controllers.import_results_controller \
    import ImportDataRow, ImportResultsController

typer_app = typer.Typer()
session = SessionLocal()
env = get_env()


@typer_app.command()
def import_results(file_path: str):
    try:
        file_path = Path(file_path)
        if not file_path.is_file():
            raise ValueError(f"File {file_path} doesn't exists")

        with open(file_path, 'r') as file:
            data = list(
                map(
                    lambda row: ImportDataRow(row),
                    list(csv.DictReader(file, delimiter=';'))
                )
            )

        controller = ImportResultsController(data, db=session)
        controller.exec()

    except Exception as e:
        print(e)
        raise typer.Abort()
