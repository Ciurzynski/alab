from sqlalchemy import Column, Integer, ForeignKey, String
from sqlalchemy.orm import relationship

from app.db import Base


class Order(Base):
    __tablename__: str = "order"

    id = Column(Integer, primary_key=True)
    items = relationship("OrderItem", uselist=True, backref="owner")
    user_id = Column(Integer, ForeignKey("user.id"))


class OrderItem(Base):
    __tablename__: str = "order_item"

    id = Column(Integer, primary_key=True)
    order_id = Column(Integer, ForeignKey("order.id"))
    name = Column(String())
    value = Column(String())
    reference = Column(String())
