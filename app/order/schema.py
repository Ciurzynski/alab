from typing import List

from pydantic import BaseModel

from app.order.models import Order, OrderItem


class ResultItemResponse(BaseModel):
    name: str
    value: str
    reference: str

    @staticmethod
    def from_model(item: OrderItem):
        return ResultItemResponse(
            name=item.name,
            value=item.value,
            reference=item.reference
        )


class OrderResponse(BaseModel):
    orderId: str
    results: List[ResultItemResponse]

    @staticmethod
    def from_model(order: Order):
        return OrderResponse(
            orderId=str(order.id),
            results=list(
                map(lambda item: ResultItemResponse.from_model(item), order.items)
            )
        )
